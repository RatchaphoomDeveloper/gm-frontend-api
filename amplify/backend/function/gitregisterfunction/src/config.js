const sql = require("mssql");

// DEV
const config = {
  user: "sa",
  password: "superpassword1fdev",
  server: "localhost", // You can use 'localhost\\instance' to connect to named instance
  database: "gm_api_dev",
};

const conn = new sql.connect(config);

module.exports = conn;
