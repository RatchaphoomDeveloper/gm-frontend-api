# FROM node:14 as build

# ARG REACT_APP_SERVICES_HOST=/services/m

# WORKDIR '/app'

# COPY ./ ./
# RUN npm run build


# FROM nginx
# COPY --from=build /app/build /usr/share/nginx/html
# RUN rm /etc/nginx/conf.d/default.conf
# COPY  nginx/nginx.conf /etc/nginx/conf.d

# EXPOSE 80
# CMD ["nginx","-g","daemon off;"]


FROM node:14-alpine

WORKDIR /app

COPY package.json .

RUN npm install

COPY . .

EXPOSE 3000

CMD [ "npm", "start" ]
