import React from "react";
import { Auth, API } from "aws-amplify";
import Routerindex from "../routers/index";
import axios from 'axios'
const Authentication = () => {
  const [login, setlogin] = React.useState(false);
  const [token, setToken] = React.useState("");
  const accessLoginState = async () => {
    Auth.currentAuthenticatedUser()
      .then((sess) => {
        // console.log(sess.signInUserSession.idToken.jwtToken);
        // setToken(sess.signInUserSession.idToken.jwtToken)
        // test(sess.signInUserSession.idToken.jwtToken);
        setlogin(true);
      })
      .catch(() => {
        console.log("not logged in");
        setlogin(false);
      });
  };

  const signOut = async () => {
    try {
      await Auth.signOut();
      setlogin(false);
    } catch (error) {
      console.log(error.message);
    }
  };

  // async function test(tokens) {
  //   const requestInfo = {
  //     headers: { Authorization : tokens}
  //   }
  //   const data = await API.get("usersprofileapi", "/userprofiles");
  //   console.log(data);
  //   axios
  //   .get("https://jsonplaceholder.typicode.com/posts/1/comments")
  //   .then(function (response) {
  //     // handle success
  //     console.log(response.data)
  //   })
  //   .catch(function (error) {
  //     // handle error
  //     console.log(error);
  //   });
  // }

  const signIn = async () => {
    try {
      const user = await Auth.signIn("gmapiadmin", "Gmpwd1234");

      setlogin(true);
    } catch (error) {
      console.log(error.message);
    }
  };

  React.useEffect(() => {
    if (login !== true) {
      signIn();
      accessLoginState();
    }
    return () => {};
  }, [0]);

  return (
    <div>
      {login === true ? (
        <section>
          <Routerindex />
        </section>
      ) : (
        <section></section>
      )}
    </div>
  );
};

export default Authentication;
