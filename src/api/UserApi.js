import axios from "axios";
import { Component } from "react";
import { WEB_API } from "../env";
import MessageStaus from "../status/message";

class UserApi extends Component {
  static login = async (data) => {
    const result = await axios({
      url: "Gm/Login",
      method: "post",
      data: data,
    }).catch((err) => {
      MessageStaus.errresmessage(err.response.data.Message);
    });
    return result.data;
  };

  static register = async (data) => {
    const result = await axios({
      url: "Gm/Register",
      method: "post",
      data: data,
    }).catch((err) => {
      console.log(err);
      MessageStaus.errresmessage(err.response.data.Message);
    });
    return result.data;
  };

  static checkUser = async (data) => {
    const result = await axios({
      url: "Gm/CheckUser",
      method: "post",
      data: data,
    }).catch((err) => {
      console.log(err);
      MessageStaus.errresmessage(err.response.data.Message);
    });
    return result.data;
  };

  static changePassword = async (data) => {
    const result = await axios({
      url: "Gm/ChangePassword",
      method: "post",
      data: data,
    }).catch((err) => {
      console.log(err);
      MessageStaus.errresmessage(err.response.data.Message);
    });
    return result.data;
  };

  static checkUserStatus = async (data, token) => {
    const result = await axios({
      url: "Gm/CheckUserStatus",
      method: "post",
      data: data,
      headers: {
        Authorization: `Bearer ${token}`,
      },
    }).catch((err) => {
      MessageStaus.errresmessage(err.response.data.Message);
    });
    return result.data;
  };

  static insUserFeedBack = async (data, token) => {
    const result = await axios({
      url: "Gm/InsUserFeedBack",
      method: "post",
      data: data,
      headers: {
        Authorization: `Bearer ${token}`,
      },
    }).catch((err) => {
      console.log(err);
      MessageStaus.errresmessage(err.response.data.Message);
    });
    return result.data;
  };
}

export default UserApi;
