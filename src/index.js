import React from "react";
import ReactDOM from "react-dom";
import "./index.scss";
import App from "./App";
import configure from "./app/store";
import { Provider } from "react-redux";
import * as serviceWorker from "./serviceWorker";
import Amplify from "aws-amplify";
import config from "./aws-exports";
import { PersistGate } from "redux-persist/integration/react";
import "./configs";
const {store,persistor} = configure()
Amplify.configure(config);

ReactDOM.render(
  <React.StrictMode>
    <Provider store={store}>
      <PersistGate loading={null} persistor={persistor}>
        <App />
      </PersistGate>
    </Provider>
  </React.StrictMode>,
  document.getElementById("root")
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
