import React from "react";
import "./index.scss";
import { Layout, Skeleton } from "antd";
import { library } from "@fortawesome/fontawesome-svg-core";
import { faBars, fas, faTimes } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { useHistory } from "react-router-dom";
import {useSelector,useDispatch}  from 'react-redux'
const { Header } = Layout;
library.add(fas);

const MainheaderIndex = () => {
  const [clicked, setClicked] = React.useState(false);
  const history = useHistory();
  const userState = useSelector((state) => state.user);
  const dispatch = useDispatch();
  const menus = [
    {
      label: "หน้าหลัก Open Data",
      url: "/",
      classNames: "nav-links",
    },
    !userState.Token ? {
      label: "สมัครเป็นผู้ใช้งาน",
      url: "/register",
      classNames: "nav-links-regis",
    }:{
      label: userState.USER_NAME,
      url: "/userprofile",
      classNames: "nav-links",
    },
    !userState.Token ? {
      label: "เข้าสู่ระบบ",
      url: "/login",
      classNames: "nav-links",
    } : {
      label: "ออกจากระบบ",
      url: "/",
      classNames: "nav-links",
    },
  ];

  React.useEffect(()=>{
    console.log(userState)
  },[])

  const logOut = () => {
    setTimeout(() => {

    }, 1500);
  }

  return (
    <div>
      <nav className="NavbarItems">
        <h1
          className="navbar-logo"
          onClick={() => {
            history.push("/");
          }}
        >
          {" "}
          <img
            src={
              "https://amplify-gmfrontapistructure-dev-91546-deployment.s3-eu-west-1.amazonaws.com/images/logo.png"
            }
            className="img-footer-left"
          />{" "}
        </h1>
        <div
          className="menu-icon"
          onClick={() => {
            setClicked(!clicked);
          }}
        >
          {clicked ? (
            <FontAwesomeIcon icon={faTimes} />
          ) : (
            <FontAwesomeIcon icon={faBars} />
          )}
        </div>
        <ul className={clicked ? " nav-menu active " : " nav-menu "}>
          {menus.map((res, i) => {
            return (
              <li key={i}>
                <label
                  className={res.classNames}
                  onClick={() => {
                    res.url != "" && history.push(res.url);
                    res.label === "ออกจากระบบ" &&
                      dispatch({
                        type: "set_user",
                        user: {},
                      });

                  }}
                >
                  {res.label}
                </label>
              </li>
            );
          })}
        </ul>
      </nav>
    </div>
  );
};

export default MainheaderIndex;
