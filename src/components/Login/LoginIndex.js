import React from "react";
import { useSelector, useDispatch } from "react-redux";
import SubFooterIndex from "../Footer/SubFooterIndex";
import { useHistory } from "react-router-dom";
import "./LoginIndex.scss";
import UserApi from "../../api/UserApi";
import Loader from "react-loader-spinner";
import Swal from "sweetalert2";
import MessageStaus from "../../status/message";

const LoginIndex = () => {
  const history = useHistory();
  const [preloader, setpreloader] = React.useState(false);
  const [username, setusername] = React.useState("");
  const [password, setpassword] = React.useState("");
  const dispatch = useDispatch();
  const login = async () => {
    try {
      if (username == "" && password == "") {
        MessageStaus.messages(-20);
      } else {
        const data = {
          UserId: username,
          UserPassword: password,
        };
        const response = await UserApi.login(data);
        if (response.UserData[0]) {
          dispatch({ type: "set_user", user: response.UserData[0] });
          setpreloader(true);
        }else{
          MessageStaus.errresmessage(response.Message)
        }
        setTimeout(async () => {
          setpreloader(false);
          history.push("/userprofile");
        }, 2000);
      }
    } catch (err) {
      setusername("")
      setpassword("")
        // alert(err.message);
    }
  };
  return (
    <div>
      {preloader === true ? (
        <section style={{ margin: "200px 0 200px 0" }}>
          <center>
            <Loader type="Puff" color="#1E90FF" height={100} width={100} />
            <label className="loading-label-spin">Loading...</label>
          </center>
        </section>
      ) : (
        <section>
          <section className="login-container">
            <h1 className="login-container-label">ลงชื่อเข้าใช้งาน</h1>
          </section>
          <section>
            <div className="login-container-input-style">
              <div className="login-item-input-1">
                <div>
                  <label className="login-container-username-label">
                    อีเมล/บัญชีผู้ใช้
                  </label>
                </div>
              </div>
              <div className="login-item-input-2">
                <input
                  type={"text"}
                  placeholder=""
                  required={true}
                  value={username}
                  onChange={(e) => setusername(e.target.value)}
                />
              </div>
            </div>
            <div className="login-container-input-style">
              <div className="login-item-input-1">
                <div>
                  <label className="login-container-username-label">
                    พาสเวิร์ด
                  </label>
                </div>
              </div>
              <div className="login-item-input-2">
                <input
                  type={"password"}
                  placeholder=""
                  required={true}
                  value={password}
                  onChange={(e) => setpassword(e.target.value)}
                />
              </div>
            </div>
          </section>
          <section>
            <div className="login-button-container">
              <button
                className="button-login login-button-item-1 "
                onClick={login}
              >
                เข้าสู่ระบบ
              </button>
              <label
                className="login-button-item-2"
                onClick={() => {
                  history.push("/changepassword");
                }}
              >
                ลืมพาสเวิร์ด
              </label>
            </div>
          </section>
        </section>
      )}

      <SubFooterIndex />
    </div>
  );
};

export default LoginIndex;
