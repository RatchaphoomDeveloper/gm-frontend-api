import React from "react";
import FooterIndex from './FooterIndex'
const SubFooterIndex = () => {
  return (
    <div>
      <section style={{backgroundColor:"#f5faff"}} >
        <div className="container-2"  >
          <div className="flex-container-section-2">
            <div className="flex-container-section-item-1-2" style={{margin:"20px 0 0 0"}}>
              <img
                src={
                  "https://amplify-gmfrontapistructure-dev-91546-deployment.s3-eu-west-1.amazonaws.com/images/approved.svg"
                }
                className="img-2"
              />
              <section style={{ margin: "10px 0 0 0" }}>
                <center>
                  <label style={{ fontStyle: "italic", fontWeight: "bold" }}>
                    สมัครเป็นผู้ใช้ APIs
                  </label>
                </center>
              </section>
              <section>
                <center>
                  <label style={{ fontStyle: "italic", fontWeight: "bold" }}>
                    เพื่อรับ TOKEN การใช้งาน
                  </label>
                </center>
              </section>
            </div>
            <div className="flex-container-section-item-2-2" style={{margin:"20px 0 0 0"}}>
              <img
                src={
                  "https://amplify-gmfrontapistructure-dev-91546-deployment.s3-eu-west-1.amazonaws.com/images/rest.svg"
                }
                className="img-3"
              />
              <section>
                <center>
                  <label style={{ fontStyle: "italic", fontWeight: "bold" }}>
                    เลือก APIs
                  </label>
                </center>
              </section>
              <section>
                <center>
                  <label style={{ fontStyle: "italic", fontWeight: "bold" }}>
                    เพื่อดูรูปแบบโพรโตคอลการเชื่อมต่อ
                  </label>
                </center>
              </section>
            </div>
            <div className="flex-container-section-item-3-2" style={{margin:"20px 0 0 0"}}>
              <img
                src={
                  " https://amplify-gmfrontapistructure-dev-91546-deployment.s3-eu-west-1.amazonaws.com/images/coding.svg"
                }
                className="img-2"
              />
              <section style={{ margin: "10px 0 0 0" }}>
                <center>
                  <label style={{ fontStyle: "italic", fontWeight: "bold" }}>
                    เขียนคำสั่ง
                  </label>
                </center>
              </section>
              <section>
                <center>
                  <label style={{ fontStyle: "italic", fontWeight: "bold" }}>
                    เรียกใช้งานตามรูปแบบที่กำหนด
                  </label>
                </center>
              </section>
            </div>
          </div>
        </div>
      </section>
      <section>
          <FooterIndex/>
      </section>
    </div>
  );
};

export default SubFooterIndex;
