import React from "react";
import "./FooterIndex.scss";

const FooterIndex = () => {
  return (
    <div>
      <footer>
        <div className="footer-container">
          <div className="big-footer-container">
            <div className="big-footer-container-item-1">
              <div className="footer-img-flex-container">
                <img
                  src={
                    "https://amplify-gmfrontapistructure-dev-91546-deployment.s3-eu-west-1.amazonaws.com/images/logo.png"
                  }
                  className="img-footer-left"
                />
              </div>
            </div>
            <div className="big-footer-container-item-2">
              <section className="label-section-1">
                <div>
                  <label className="label-footer-1">
                    นโยบายการคุ้มครองข้อมูลส่วนบุคคล
                  </label>
                  <label className="label-footer-1">
                    แนวทางปฏิบัติการเผยแพร่ข้อมูลต่อสาธารณะ
                  </label>
                  {/* <label className="">
                    ส่งข้อเสนอแนะ | ถาม-ตอบ | เงื่อนไขและข้อตกลง |
                    ความเป็นส่วนตัว | เกี่ยวกับเรา
                  </label> */}
                </div>
              </section>
              <section className="label-section-2">
                <label>
                  Copyright © 2021 The Gem and Jewelry Institute of Thailand
                  (Public Organization). All rights reserved. 140, 140/1-3,
                  140/5 อาคารไอทีเอฟ ทาวเวอร์ ชั้น 1-4 และชั้น 6 ถนนสีลม
                  แขวงสุริยวงศ์ เขตบางรัก กรุงเทพฯ 10500 โทรศัพท์ 0 2634 4999
                  โทรสาร 0 2634 4970 อีเมล jewelry@git.com
                </label>
              </section>
            </div>
          </div>
        </div>
      </footer>
    </div>
  );
};

export default FooterIndex;
