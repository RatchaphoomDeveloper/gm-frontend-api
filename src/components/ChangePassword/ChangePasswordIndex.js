import React from "react";
import SubFooterIndex from "../Footer/SubFooterIndex";
import { useHistory } from "react-router-dom";
import "./ChangePasswordIndex.scss";
import UserApi from "../../api/UserApi";
import SpinLoader from "../../materials/loader/SpinLoader";
import MessageStaus from "../../status/message";
const ChangePasswordIndex = () => {
  const history = useHistory();
  const [changepwdFlag, setchangepwdFlag] = React.useState(false);
  const [userid, setuserid] = React.useState("");
  const [userpassword, setuserpassword] = React.useState("");
  const [userconfirmpassword, setuserconfirmpassword] = React.useState("");
  const [userData, setuserData] = React.useState({});
  const [loader, setloader] = React.useState(false);

  const onCheckUser = async () => {
    try {
      const data = {
        UserId: userid,
        UserPassword: "",
      };
      const response = await UserApi.checkUser(data);
      if (response) {
        setuserData(response.UserData[0]);
        setloader(true);
      }

      setTimeout(() => {
        setchangepwdFlag(true);
        setloader(false);
      }, 1500);
    } catch (err) {
      setuserid("");
      setuserpassword("");
      setuserconfirmpassword("");
    }
  };
  const onCheckPassword = async () => {
    try {
      if (userconfirmpassword !== userpassword) {
        MessageStaus.messages(-22)
      } else {
        const data = {
          UserId: userData.UUID,
          UserPassword: userpassword,
        };
        const response = await UserApi.changePassword(data);
        if (response) {
          setuserData(response.Message);
          setloader(true);
        }
  
        setTimeout(() => {
          setchangepwdFlag(false);
          setloader(false);
        }, 1500);
      }
    } catch (err) {
      setuserid("");
      setuserpassword("");
      setuserconfirmpassword("");
    }
  };

  React.useEffect(() => {
    if (userData.UUID == undefined) {
      setchangepwdFlag(false);
    }
  }, []);
  return (
    <div>
      {loader === false ? (
        <section>
          <section className="login-container">
            <h1 className="login-container-label">ลืมรหัสผ่าน</h1>
          </section>
          <section>
            {changepwdFlag === false ? (
              <div className="login-container-input-style">
                <div className="login-item-input-1">
                  <div>
                    <label className="login-container-username-label">
                      อีเมล
                    </label>
                  </div>
                </div>
                <div className="login-item-input-2">
                  <input
                    type={"email"}
                    placeholder=""
                    value={userid}
                    onChange={(e) => setuserid(e.target.value)}
                  />
                </div>
              </div>
            ) : (
              <div>
                <div className="login-container-input-style">
                  <div className="login-item-input-1">
                    <div>
                      <label className="login-container-username-label">
                        พาสเวิร์ด
                      </label>
                    </div>
                  </div>
                  <div className="login-item-input-2">
                    <input
                      type={"password"}
                      placeholder=""
                      value={userpassword}
                      onChange={(e) => setuserpassword(e.target.value)}
                    />
                  </div>
                </div>
                <div className="login-container-input-style">
                  <div className="login-item-input-1">
                    <div>
                      <label className="login-container-username-label">
                        ยืนยันพาสเวิร์ด
                      </label>
                    </div>
                  </div>
                  <div className="login-item-input-2">
                    <input
                      type={"password"}
                      placeholder=""
                      value={userconfirmpassword}
                      onChange={(e) => setuserconfirmpassword(e.target.value)}
                    />
                  </div>
                </div>
              </div>
            )}
          </section>
          <section>
            <div className="change-password-button-container">
              <button
                className="button-login login-button-item-1 "
                onClick={() => {
                  if (changepwdFlag === false) {
                    onCheckUser();
                  } else {
                    onCheckPassword()
                  }
                }}
              >
                ยืนยัน
              </button>
            </div>
          </section>
        </section>
      ) : (
        <SpinLoader />
      )}

      <SubFooterIndex />
    </div>
  );
};

export default ChangePasswordIndex;
