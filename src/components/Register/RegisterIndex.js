import React from "react";
import SubFooterIndex from "../Footer/SubFooterIndex";
import Recaptcha from "react-recaptcha";
import { useSelector } from "react-redux";
import "./RegisterIndex.scss";
import Loader from "react-loader-spinner";
import MessageStaus from "../../status/message";
import NotFound from "../NotFound/NotFound";
import UserApi from "../../api/UserApi";
import { useHistory } from "react-router-dom";

const RegisterIndex = () => {
  const history = useHistory()
  const [company, setcompany] = React.useState("");
  const [userid, setuserid] = React.useState("");
  const [useremail, setuseremail] = React.useState("");
  const [username, setusername] = React.useState("");
  const [userlastname, setuserlastname] = React.useState("");
  const [userpassword, setuserpassword] = React.useState("");
  const [userconfirmpass, setuserconfirmpass] = React.useState("");
  const [recaptoken, setrecaptoken] = React.useState("sdfsdf");
  const [errFlag, seterrFlag] = React.useState(false);
  const [loader, setLoader] = React.useState(false);
  const userData = useSelector((state) => state.user);
  // specifying your onload callback function
  var callback = function () {
    console.log("Done!!!!");
  };

  // specifying verify callback function
  var verifyCallback = function (response) {
    setrecaptoken(response);
  };

  const onRegister =  async() => {
    try {
      if (
        company == "" ||
        userid == "" ||
        userpassword == "" ||
        userlastname == "" ||
        username == "" ||
        userconfirmpass == ""
      ) {
        MessageStaus.messages(-20);
      }
      if (/^[a-zA-Z0-9]+$/.test(userid) === false && userid !== "") {
        MessageStaus.messages(-21);
      }
      if (
        /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/.test(
          useremail
        ) === false &&
        useremail !== ""
      ) {
        MessageStaus.messages(-21);
      }
      if (userpassword !== userconfirmpass) {
        MessageStaus.messages(-22);
      }
      if (
        company !== "" &&
        userid !== "" &&
        userpassword !== "" &&
        userlastname !== "" &&
        username !== "" &&
        userconfirmpass !== ""
      ) {
        try {
          const data = {
            ID: 0,
            UUID: "",
            USER_ID: userid,
            USER_PASSWORD: userpassword,
            USER_EMAIL: useremail,
            USER_NAME: username,
            USER_LASTNAME: userlastname,
            USER_COMAPANY: company,
          };
          const response = await UserApi.register(data);
          if (response) {
            setuserid("");
            setcompany("");
            setuserconfirmpass("");
            setuserpassword("");
            setuseremail("");
            setusername("");
            setuserlastname("");
            MessageStaus.successmessage(response.Message);
            setLoader(true);
          }
          setTimeout(() => {
            setLoader(false);
            history.push("/login")
          }, 1500);
        } catch (err) {
          setuserid("");
          setcompany("");
          setuserconfirmpass("");
          setuserpassword("");
          setuseremail("");
          setusername("");
          setuserlastname("");
        }
      }
    } catch (err) {
      setuserid("");
      setcompany("");
      setuserconfirmpass("");
      setuserpassword("");
      setuseremail("");
      setusername("");
      setuserlastname("");
    }
  };
  return (
    <div>
      {userData.Token ? (
        <NotFound />
      ) : (
        <section>
          <section>
            <div className="register-container">
              <h1 className="register-label">
                ลงทะเบียนเพื่อรับ APIs Key ใช้งานฟรี
              </h1>
            </div>
          </section>
          <section>
            <div className="register-hilight-container">
              <div className="register-hilight-container-flex">
                <div className="register-hilight-item-1">
                  <label className="register-input-label">
                    หน่วยงาน/บริษัท/อื่นๆ
                  </label>
                </div>
                <div className="register-hilight-item-2 input-container ">
                  <input
                    className="input-style"
                    type="text"
                    placeholder="Organization/Company/Other"
                    required={true}
                    value={company}
                    onChange={(e) => setcompany(e.target.value)}
                  />
                </div>
              </div>
              <div className="register-hilight-container-flex">
                <div className="register-hilight-item-1">
                  <label className="register-input-label">บัญชีผู้ใช้</label>
                </div>
                <div className="register-hilight-item-2 input-container ">
                  <input
                    className="input-style"
                    type="text"
                    placeholder=" อนุญาตเฉพาะ A-Z, a-z, 0-9, '_'"
                    required={true}
                    value={userid}
                    onChange={(e) => setuserid(e.target.value)}
                  />
                </div>
              </div>
              {userid !== "" && (
                <div className="register-section-recapt-container-err">
                  <label>
                    {" "}
                    {/^[a-zA-Z0-9]+$/.test(userid) === false &&
                      "* รูปแบบบัญชีผู้ใช้งานผิด ( test123T )"}
                  </label>
                </div>
              )}
              <div className="register-hilight-container-flex">
                <div className="register-hilight-item-1">
                  <label className="register-input-label">อีเมล</label>
                </div>
                <div className="register-hilight-item-2 input-container ">
                  <input
                    className="input-style"
                    type="email"
                    placeholder="Email"
                    required={true}
                    value={useremail}
                    onChange={(e) => setuseremail(e.target.value)}
                  />
                </div>
              </div>
              {useremail !== "" && (
                <div className="register-section-recapt-container-err">
                  <label>
                    {" "}
                    {/^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/.test(
                      useremail
                    ) === false && "* รูปแบบอีเมล์ผิด ( test@email.com )"}
                  </label>
                </div>
              )}

              {/* /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/ */}
              <div className="register-hilight-container-flex">
                <div className="register-hilight-item-1">
                  <label className="register-input-label">ชื่อ</label>
                </div>
                <div className="register-hilight-item-2 input-container ">
                  <input
                    className="input-style"
                    type="text"
                    placeholder="First name"
                    required={true}
                    value={username}
                    onChange={(e) => setusername(e.target.value)}
                  />
                </div>
              </div>
              <div className="register-hilight-container-flex">
                <div className="register-hilight-item-1">
                  <label className="register-input-label">นามสกุล</label>
                </div>
                <div className="register-hilight-item-2 input-container ">
                  <input
                    className="input-style"
                    type="text"
                    placeholder="Last name"
                    required={true}
                    value={userlastname}
                    onChange={(e) => setuserlastname(e.target.value)}
                  />
                </div>
              </div>
            </div>
            <div className="register-none-hilight-container">
              <div className="register-hilight-container-flex">
                <div className="register-hilight-item-1">
                  <label className="register-input-label">พาสเวิร์ด</label>
                </div>
                <div className="register-hilight-item-2 input-container ">
                  <input
                    className="input-style"
                    type="password"
                    placeholder="Password"
                    required={true}
                    value={userpassword}
                    onChange={(e) => setuserpassword(e.target.value)}
                  />
                </div>
              </div>
              <div className="register-hilight-container-flex">
                <div className="register-hilight-item-1">
                  <label className="register-input-label">
                    ยืนยันพาสเวิร์ด
                  </label>
                </div>
                <div className="register-hilight-item-2 input-container ">
                  <input
                    className="input-style"
                    type="password"
                    placeholder="Confirm password"
                    required={true}
                    value={userconfirmpass}
                    onChange={(e) => setuserconfirmpass(e.target.value)}
                  />
                </div>
              </div>
            </div>
            {userpassword.length !== userconfirmpass.length ? (
              <div className="register-section-recapt-container-err">
                {userpassword.toString() === userconfirmpass.toString() ? (
                  <label> </label>
                ) : (
                  <label>* รหัสผ่านไม่ตรงกัน</label>
                )}
              </div>
            ) : (
              <div className="register-section-recapt-container-err">
                {userpassword.toString() === userconfirmpass.toString() ? (
                  <label> </label>
                ) : (
                  <label>* รหัสผ่านไม่ตรงกัน</label>
                )}
              </div>
            )}

            <div className="register-section-recapt-container">
              <label className="regist-detail-label">
                ข้อตกลงในการใช้งาน API 1. ระบบจะมีการเก็บข้อมูลผู้ใช้งาน
                เพื่อการวิเคราะห์ปริมาณการใช้งานและปรับปรุงประสิทธิภาพ 2. ระบบ
                API จะอนุญาติให้เรียกใช้งานได้สูงสุดวันละ 1000 ครั้ง/ผู้ใช้งาน
              </label>
            </div>
            {/* <div className="register-section-recapt-container">
              <Recaptcha
                sitekey={"6Le645gaAAAAAKU3fXGQ5hHclpG7W5nD2MnOLoJN"}
                render="explicit"
                onloadCallback={callback}
                verifyCallback={verifyCallback}
              />
            </div> */}
            <div className="register-section-recapt-container">
              <button
                className="button-login recaptcha-container"
                onClick={onRegister}
                style={{
                  background: recaptoken === "" ? "#ff8080" : "",
                }}
                disabled={recaptoken !== "" ? false : true}
              >
                <div style={{ display: "flex" }}>
                  {loader === true ? (
                    <label style={{ margin: "7px 0 0 0" }}>ลงทะเบียน</label>
                  ) : (
                    <label>ลงทะเบียน</label>
                  )}
                  {loader === true && (
                    <div style={{ margin: "7px 0 0 15px" }}>
                      <Loader
                        type="Circles"
                        color="#C6E4EC"
                        height={25}
                        width={25}
                      />
                    </div>
                  )}
                </div>
              </button>
            </div>
          </section>
        </section>
      )}

      <SubFooterIndex />
    </div>
  );
};

export default RegisterIndex;
