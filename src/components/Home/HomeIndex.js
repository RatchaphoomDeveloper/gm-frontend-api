import React from "react";
import { Parallax } from "react-parallax";
import FooterIndex from '../Footer/FooterIndex'
import "./HomeIndex.scss";

const HomeIndex = () => {
  return (
    <div>

      <div
        style={{
          position: "relative",
          transform: " translate(0%, -40%)",
          zIndex: -1,
          width: "100%",
        }}
      >
        <Parallax
          bgImage={
            "https://amplify-gmfrontapistructure-dev-91546-deployment.s3-eu-west-1.amazonaws.com/images/henning-witzel-ukvgqriuOgo-unsplash.jpg"
          }
          strength={400}
        >
          <div style={{ height: 1000 }}>
            <div className="inside-parallax">
              <h1 className="h1-color">
                อีกก้าวหนึ่งของการมุ่งมั่นไปสู่ความเป็นเลิศด้านการบริการข้อมูลเปิดของหน่วยงานภาครัฐในรูปแบบ
                APIs
              </h1>
            </div>
          </div>
        </Parallax>
      </div>
      <div className="welcome-container">
        <h1 className="welcome-label">ยินดีต้อนรับผู้ใช้งาน API</h1>
        <ul class="flex-container">
          <li class="flex-item">
            <div style={{ marginTop: "100px" }}>
              <h1 className="h3-text-1">มาร่วมกันเป็นกลุ่มผู้ใช้ API</h1>
              <label className="label-text-1">
                สมัครสมาชิกเพื่อขอใช้งาน API
              </label>
            </div>

            <div style={{ marginTop: "100px" }}>
              <h1 className="h3-text-2">มาร่วมกันเป็นกลุ่มผู้ใช้ API</h1>
              <label className="label-text-2">
                เพื่อรับ Token สำหรับใช้ API
              </label>
            </div>
          </li>
          <li class="flex-item">
            <img
              src={
                "https://amplify-gmfrontapistructure-dev-91546-deployment.s3-eu-west-1.amazonaws.com/images/hellobg.jpg"
              }
              className="welcome-img"
            />
          </li>
          <li class="flex-item">
            {" "}
            <div style={{ marginTop: "100px" }}>
              <h1 className="h3-text-3">มาร่วมกันเป็นกลุ่มผู้ใช้ API</h1>
              <label className="label-text-3">
                ดูรูปแบบโพรโทคอลการเชื่อมต่อและตัวอย่างการใช้งานที่
                คู่มือปฏิบัติ
              </label>
            </div>
            <div style={{ marginTop: "100px" }}>
              <h1 className="h3-text-4">มาร่วมกันเป็นกลุ่มผู้ใช้ API</h1>
              <label className="label-text-4">
                เขียนคำสั่งตามรูปแบบที่กำหนด
                และตรวจสอบความถูกต้องเพื่อรอรับผลลัพธ์
              </label>
            </div>
          </li>
          {/* <li class="flex-item">4</li> */}
        </ul>
        <div>
          <center>
            <div className="label-text-5-container">
              <center>
                <label>
                  ผู้ขอใช้บริการข้อมูลสามารถใช้บริการโดยการเชื่อมต่อมายังเว็บ
                  api.data.go.th ซึ่งเป็นการให้บริการ
                  ในรูปแบบของเว็บเซอร์วิสเทคโนโลยี หรือเว็บเอพีไอ ประเภท RESTful
                  และกำหนด โพรโทคอลการ เชื่อมต่อเอพีไอในรูปแบบ REST
                  โดยผู้ขอใช้บริการข้อมูลจำเป็นต้องดำเนินการเขียนคำสั่งการเชื่อม
                  ต่อที่ถูกต้อง และเหมาะสมกับการเชื่อมต่อในรูปแบบที่กำหนด
                </label>
              </center>
            </div>
          </center>
        </div>
        <section>
          <div class="flex-containers">
            <div class="flex-item-left" style={{ color: "black" }}>
              <h1 className="flex-item-left-label">
                เรารู้สึกดีเสมอเมื่อ คุณสนใจเป็นผู้ใช้ APIs
              </h1>
            </div>
            {/* <div style={{borderLeft:"4px solid #ffffff"}} className="flex-item-left" ></div> */}
            <div class="flex-item-right" style={{ color: "black" }}>
              <label className="flex-item-right-label">
                การเปิดให้บริการข้อมูลผ่านเครือข่าย ในรูปแบบ Data API
                เพื่อให้นักพัฒนาแอปพลิเคชัน ผู้ใช้บริการ ภาคธุรกิจเอกชน
                และหน่วยงานต่างๆ ตลอดจนประชาชนทั่วไป
                สามารถเข้าถึงข้อมูลเปิดได้สะดวกรวดเร็ว และสอดคล้องตามแนวทางสากล
                ท่านสามารถดูรายการชุดข้อมูลและใช้งาน Data API ของชุดข้อมูล
                (เฉพาะชุดข้อมูลที่อยู่ในรูปแบบที่เหมาะสม) ได้จากเว็บ{" "}
                <a href="#"> Data.go.th </a>
                ดังมีรายละเอียดการใช้งาน API ใน <a href="#">คู่มือการใช้งาน</a>
              </label>
            </div>
          </div>
        </section>
        <section>
          <div className="container-1">
            <h1 className="welcome-label">การเปิดชุดข้อมูลแบบ APIs</h1>
            <div className="flex-container-section-1">
              <div className="flex-container-section-item-1">
                <img
                  src={
                    "https://amplify-gmfrontapistructure-dev-91546-deployment.s3-eu-west-1.amazonaws.com/images/profile.png"
                  }
                  className="img-profile"
                />
                <section>
                  <label>หน่วยงานจัดเตรียมชุดข้อมูล</label>
                </section>
                <section style={{ marginBottom: "70px" }}>
                  <a href="#">คุณสมบัติข้อมูลเปิด ?</a>
                </section>
              </div>
              <div className="flex-container-section-item-2">
                <img
                  src={
                    "https://amplify-gmfrontapistructure-dev-91546-deployment.s3-eu-west-1.amazonaws.com/images/tag.png"
                  }
                  className="img-profile"
                />{" "}
                <section>
                  <label>จัดทำ Metadata</label>
                </section>
                <section style={{ marginBottom: "70px" }}>
                  <a href="#">คำอธิบายชุดข้อมูล ?</a>
                </section>
              </div>
              <div className="flex-container-section-item-3">
                <img
                  src={
                    "https://amplify-gmfrontapistructure-dev-91546-deployment.s3-eu-west-1.amazonaws.com/images/mail.png"
                  }
                  className="img-profile"
                />
                <section>
                  <label>ส่งข้อมูลมาที่ DGA</label>
                </section>
                <section style={{ marginBottom: "70px" }}>
                  <a href="#">contact@dga.or.th</a>
                </section>
              </div>
            </div>
          </div>
        </section>
        <section>
          <center>
            <label className="api-label-1">
              กรณีหน่วยงานมีบริการชุดข้อมูลเปิดแบบ APIs
              สามารถนำส่งเผยแพร่บนเว็บไซต์ โดยส่งมาที่ contact@dga.or.th
            </label>
          </center>
        </section>
        <section>
          <div className="container-2">
            <div className="flex-container-section-2">
              <div className="flex-container-section-item-1-2">
                <img
                  src={
                    "https://amplify-gmfrontapistructure-dev-91546-deployment.s3-eu-west-1.amazonaws.com/images/approved.svg"
                  }
                  className="img-2"
                />
                <section style={{ margin: "10px 0 0 0" }}>
                  <center>
                    <label style={{ fontStyle: "italic", fontWeight: "bold" }}>
                      สมัครเป็นผู้ใช้ APIs
                    </label>
                  </center>
                </section>
                <section>
                  <center>
                    <label style={{ fontStyle: "italic", fontWeight: "bold" }}>
                      เพื่อรับ TOKEN การใช้งาน
                    </label>
                  </center>
                </section>
              </div>
              <div className="flex-container-section-item-2-2">
                <img
                  src={
                    "https://amplify-gmfrontapistructure-dev-91546-deployment.s3-eu-west-1.amazonaws.com/images/rest.svg"
                  }
                  className="img-3"
                />
                <section>
                  <center>
                    <label style={{ fontStyle: "italic", fontWeight: "bold" }}>
                      เลือก APIs
                    </label>
                  </center>
                </section>
                <section>
                  <center>
                    <label style={{ fontStyle: "italic", fontWeight: "bold" }}>
                      เพื่อดูรูปแบบโพรโตคอลการเชื่อมต่อ
                    </label>
                  </center>
                </section>
              </div>
              <div className="flex-container-section-item-3-2">
                <img
                  src={
                    " https://amplify-gmfrontapistructure-dev-91546-deployment.s3-eu-west-1.amazonaws.com/images/coding.svg"
                  }
                  className="img-2"
                />
                <section style={{ margin: "10px 0 0 0" }}>
                  <center>
                    <label style={{ fontStyle: "italic", fontWeight: "bold" }}>
                      เขียนคำสั่ง
                    </label>
                  </center>
                </section>
                <section>
                  <center>
                    <label style={{ fontStyle: "italic", fontWeight: "bold" }}>
                      เรียกใช้งานตามรูปแบบที่กำหนด
                    </label>
                  </center>
                </section>
              </div>
            </div>
          </div>
        </section>
        <section style={{ color: "black" }}>
          <FooterIndex/>

        </section>
      </div>
    </div>
  );
};

export default HomeIndex;
