import React from "react";
import SubFooterIndex from "../Footer/SubFooterIndex";
import { isMobile } from "react-device-detect";
import Alert from "../../materials/alerts/Alert";
import Amplify, { Auth, API } from "aws-amplify";
import { useSelector, useDispatch } from "react-redux";
import config from "../../aws-exports";
import "./UserProfileIndex.scss";
import { WEB_API } from "../../env";
import MessageStaus from "../../status/message";
import UserApi from "../../api/UserApi";
import Loader from "react-loader-spinner";
Amplify.configure(config);

const UserProfileIndex = () => {
  const userState = useSelector((state) => state.user);
  const [feedback, setFeedBack] = React.useState("");
  const [statusData, setstatusData] = React.useState([]);
  const [loader, setloader] = React.useState(false);
  const fetchuserprofileTest = async () => {
    const data = await API.get("usersprofileapi", "/userprofiles");
    console.log(data);
  };

  const sendFeedBack = async () => {
    try {
      if (feedback === "") {
        MessageStaus.messages(-20);
      } else {
        const data = {
          UUID: userState.UUID,
          FEEDBACK: feedback,
        };
        const response = await UserApi.insUserFeedBack(data, userState.Token);
        if (response) {
          setloader(true);
          setTimeout(() => {
            MessageStaus.successmessage(response.Message);
            setloader(false);
            setFeedBack("");
          }, 1500);
        }
      }
    } catch (err) {
      setFeedBack("");
    }
  };

  const getApproveStatus = async () => {
    try{
      const data = {
        UUID: userState.UUID,
        USER_NAME: "string",
      };
      const respose = await UserApi.checkUserStatus(data, userState.Token);
      if(respose){
        setstatusData(respose.UserStatusData[0]);
      }

      console.log(respose);

    }catch(err){

    }
  };

  React.useEffect(() => {
    fetchuserprofileTest();
    getApproveStatus()
    return () => {};
  }, [0]);
  return (
    <div>
      <section>
        {isMobile ? (
          <div>
            <Alert
              dialog={"Warning"}
              text={"Please open in desktop"}
              color={"warning"}
              bgcolor={""}
              textColor={""}
            />
          </div>
        ) : (
          <div>
            <section>
              {/* alert
                  1 danger = color={""}
                  2 success = color={"success"}
                  3 warning = color={"warning"}
                  4 info = color={"info"}
              */}
              <Alert
                dialog={"Success"}
                text={"Login success"}
                color={"success"}
                bgcolor={""}
                textColor={""}
              />
            </section>
            <center>
              <section>
                <div className="table-container">
                  <table id="customers">
                    <tr>
                      <th>บริการ APIs</th>
                      <th>แผนการบริการ API</th>
                      <th>USER TOKEN</th>
                      <th>USER MANUALS</th>
                    </tr>
                    <tr>
                      <td>BWC</td>
                      <td>
                        <label>Open-Search</label>
                      </td>
                      <td>
                        {statusData.SHORT_STATUS === "A"
                          ? userState.Token
                          : statusData.APPROVE_STATUS + ` (${statusData.THAI_APPROVE_STATUS})`}
                      </td>
                      <td>
                        {" "}
                        <a href={WEB_API + "index.html"}>
                          {WEB_API + "index.html"}
                        </a>{" "}
                      </td>
                    </tr>
                  </table>
                </div>
              </section>
              <section
                style={{ marginTop: "20px" }}
                className="feedback-container"
              >
                <h3 style={{ fontWeight: "600" }}>ติดต่อ Admin</h3>
                <div>
                  <input
                    type={"text"}
                    style={{ marginRight: "20px" }}
                    value={feedback}
                    onChange={(e) => setFeedBack(e.target.value)}
                  />
                  <span>
                    <button className="button-feedback" onClick={sendFeedBack}>
                      <div style={{ display: "flex" }}>
                        {loader === true ? (
                          <label>ลงทะเบียน</label>
                        ) : (
                          <label>ลงทะเบียน</label>
                        )}
                        {loader === true && (
                          <div style={{ margin: "3px 0 0 15px" }}>
                            <Loader
                              type="Circles"
                              color="#C6E4EC"
                              height={15}
                              width={15}
                            />
                          </div>
                        )}
                      </div>
                    </button>
                  </span>
                </div>
              </section>
            </center>
          </div>
        )}
      </section>
      <SubFooterIndex />
    </div>
  );
};

export default UserProfileIndex;
