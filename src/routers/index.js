import React from "react";
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";
import MainheaderIndex from "../components/Mainheader/MainheaderIndex";
import HomeIndex from "../components/Home/HomeIndex";
import LoginIndex from "../components/Login/LoginIndex";
import RegisterIndex from "../components/Register/RegisterIndex";
import ChangePasswordIndex from "../components/ChangePassword/ChangePasswordIndex";
import UserProfileIndex from "../components/UserProfile/UserProfileIndex";
import { useSelector, useDispatch } from "react-redux";
import NotFound from "../components/NotFound/NotFound";
const Routerindex = () => {
  const userData = useSelector((state) => state.user);
  return (
    <div>
      <Router>
        <MainheaderIndex />
        <Switch>
          <Route exact path={"/"} component={HomeIndex} />
          <Route path={"/login"} component={LoginIndex} />
          <Route path={"/Register"} component={RegisterIndex} />
          <Route path={"/changepassword"} component={ChangePasswordIndex} />
          {userData.Token ? (
            <section>
              <Route path={"/userprofile"} component={UserProfileIndex} />
            </section>
          ) : (
            <Route component={NotFound} />
          )}
        </Switch>
      </Router>
    </div>
  );
};

export default Routerindex;
