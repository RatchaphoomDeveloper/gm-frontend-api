import React from "react";
import Loader from "react-loader-spinner";

const SpinLoader = () => {
  return (
    <div>
      <section style={{ margin: "200px 0 200px 0" }}>
        <center>
          <Loader type="Puff" color="#1E90FF" height={100} width={100} />
          <label className="loading-label-spin">Loading...</label>
        </center>
      </section>
    </div>
  );
};

export default SpinLoader;
