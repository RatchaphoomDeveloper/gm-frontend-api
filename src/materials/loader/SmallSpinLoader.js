import React from "react";
import Loader from "react-loader-spinner";

const SmallSpinLoader = () => {
  return (
    <div>
      <section style={{ margin: "7px 0 0 15px" }}>
        <center>
          <Loader type="Circles"
                        color="#C6E4EC"
                        height={25}
                        width={25} />
        </center>
      </section>
    </div>
  );
};

export default SmallSpinLoader;
