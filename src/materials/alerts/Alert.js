import React from "react";
import {
  faExclamationCircle,
  faTimes,
} from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import "./Alert.scss";
const Alert = ({ dialog, text, color, bgColor }) => {
  const [show, setShow] = React.useState(true);
  const showNotify = () => {
    setShow(false);
    alert("sdfsdfsdfsdf");
  };
  return (
    <div>
      {/* alert
                  1 danger = color={""}
                  2 success = color={"success"}
                  3 warning = color={"warning"}
                  4 info = color={"info"}
              */}
      <div
        className={`alert ${color}`}
        style={{
          display: show === true ? " " : "none",
          transition: show === true ? "opacity 0.6s" : "opacity 0.6s",
        }}
      >
        <span
          className="closebtn"
          onClick={() => {
            setShow(false);
          }}
        >
          &times;
        </span>
        <strong>{dialog}!</strong> {text}
      </div>
    </div>
  );
};

export default Alert;
