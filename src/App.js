import React from 'react';
// import logo from './logo.svg';
// import { Counter } from './features/counter/Counter';
import Authentication from "./utils/Authentication";
import './App.scss';
import "antd/dist/antd.css";

function App() {
  return (
    <div>
        <Authentication/>
    </div>
  );
}

export default App;
